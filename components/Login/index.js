import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Copyright from "../CopyRight";
import Google from "../../icons/Google";
import Facebook from "../../icons/Facebook";
import styled from "styled-components";

const LoginHeader = styled.div`
  display: flex;
  justify-content: center;
`;

const AvatarWrapper = styled(Avatar)`
    && {
        background-color:  ${({color}) => color };
        margin: 5px;
        cursor: pointer;
    }
`

const useStyles = makeStyles((theme) => ({
  loginContainer: {
    maxWidth: "320px",
    margin: "100px auto",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  neuchaFont: {
    fontFamily: `Neucha !important`,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = ({ loggedIn, logout, login }) => {
  const classes = useStyles();

  return (
    <div className={classes.loginContainer}>
        <LoginHeader>
            <AvatarWrapper color="red">
                    <Google />
            </AvatarWrapper>
              <AvatarWrapper color="#007bff">
                    <Facebook />
            </AvatarWrapper>
        </LoginHeader>
      <Typography component="h1" variant="h5" className={classes.neuchaFont}>
        Sign in
      </Typography>
      <form className={classes.form} noValidate>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
          className={classes.neuchaFont}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          className={classes.neuchaFont}
        />
        <FormControlLabel
            control={<Checkbox value="remember" color="primary" className={classes.neuchaFont}/>}
            label="Remember me"
            className={classes.neuchaFont}
        />
        <Button
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          onClick={login}
          className={classes.neuchaFont}
        >
          Sign In
        </Button>
        <Grid container>
          <Grid item xs>
            <Link href="#" variant="body2" className={classes.neuchaFont}>
              Forgot password?
            </Link>
          </Grid>
          <Grid item>
                      <Link href="#" variant="body2" className={classes.neuchaFont}>
              {"Don't have an account? Sign Up"}
            </Link>
          </Grid>
        </Grid>
        <Box mt={5}>
          <Copyright />
        </Box>
      </form>
    </div>
  );
};

export default Login;
