import HeadMain from './Head'
import LayoutMain from './LayoutMain'

const Layout = ({ children }) =>
    <main className="main-container">
        <HeadMain />
        <LayoutMain children={children} />
    </main>

export default Layout