
import Navigation from '../Navigation'

const LayoutMain = ({ children }) =>
    <div className="main-layout">
        <Navigation />
        <div className="main-page">
            {children}
        </div>
    </div>

export default LayoutMain