import Link from 'next/link'
import styled from "styled-components";

const NavigationWrapper = styled.nav`
    display: flex;
    justify-content: space-between;
    margin: 0 20px;
    .navbar-collapse{
        flex: 1;
        .navbar-nav{
            display: flex;
            justify-content: flex-end;
            width: 100%;
        }
    }
`

const Navigation = () =>
    <NavigationWrapper className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#">ErickNext</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="main-navigation__list navbar-nav">
                <li className="main-navigation__item nav-item active"><Link href="/"><a  className="nav-link">Home</a></Link></li>
                <li className="main-navigation__item nav-item"><Link href="/about"><a  className="nav-link">About</a></Link></li>
                <li className="main-navigation__item nav-item"><Link href="/services"><a  className="nav-link">Services</a></Link></li>
            </ul>
        </div>
    </NavigationWrapper>

export default Navigation